#!powershell

## An Ansible XML manipulator for windows host management.

# WANT_JSON
# POWERSHELL_COMMON

$params = Parse-Args $args;

$file = Get-Attr $params "file" -failifempty $true
$xpath = Get-Attr $params "xpath" $Null  # "/file/foo[@attribute='bar']/@attribute"
$value = Get-Attr $params "value" $Null  # "new value"
$state = Get-Attr $params "state" $Null
$addContainer = Get-Attr $params "addContainer" $Null


# Function to edit nodes
function Edit-XmlNodes 
{
    param 
    (
        [xml] $doc = $(throw "xml input missing"),
        [string] $xpath = $(throw "missing xpath"),
        [string] $val = $(throw "missing value")
    )    
    $nodes = $doc.SelectNodes($xpath)
     
    foreach ($node in $nodes) {
        if ($node -ne $null) {
            if ($node.NodeType -eq "Element") {
                $node.InnerXml = $value
            }
            else {
                $node.Value = $value
            }
        }
    }
}

# Given an xml element, inserts an xml fragment in the form of a string
# e.g  Add-XmlFragment $xml.configuration.connectionStrings "<add name='String' connectionString='...' providerName='...'>"
function Add-XmlFragment 
{
    Param(        
        [Parameter(Mandatory=$true)][System.Xml.XmlNode] $xmlElement,
        [Parameter(Mandatory=$true)][string] $text
    )        
    $xml = $xmlElement.OwnerDocument.ImportNode(([xml]$text).DocumentElement, $true)
    [void]$xmlElement.AppendChild($xml)
}

function Clear-Nodes 
{
    param 
    (
        [xml] $doc = $(throw "xml input missing"),
        [string] $xpath = $(throw "missing xpath")
    )

    $ChildNodes = $doc.SelectNodes($xpath)

    foreach($Child in $ChildNodes)
    {
        [void]$Child.ParentNode.RemoveChild($Child)
    }
}

# Function to create container nodes
function add-Container
{
    param 
    (
        [xml] $doc = $(throw "xml input missing"),
        [string] $xpath = $(throw "missing xpath"),
        [string] $val = $(throw "missing value")
    )
    
    $node = $doc.SelectSingleNode($xpath)
    $newNode = $doc.CreateElement($val);
    $node.AppendChild($newNode)
}

# Function to add children - from string
function add-XmlNodes 
{
    param 
    (
        [xml] $doc = $(throw "xml input missing"),
        [string] $xpath = $(throw "missing xpath"),
        [string] $val = $(throw "missing value")
    )
    
    $node = $doc.SelectSingleNode($xpath)
    Add-XmlFragment $node $val
}

# Function to remove children
function remove-XmlNodes 
{
    param 
    (
        [xml] $doc = $(throw "xml input missing"),
        [string] $xpath = $(throw "missing xpath"),
        [string] $val = $(throw "missing value")
    )    
    $nodes = $doc.SelectNodes($xpath)
     
    foreach ($node in $nodes) {
        if ($node -ne $null) {
            if ($node.NodeType -eq "Element") {
                $node.InnerXml = $value
            }
            else {
                $node.Value = $value
            }
        }
    }
}

# Script Start
$result = New-Object psobject @{
    changed = $false
};

# Get the xml doc
if (-Not (Test-Path $file)) # check file exists if not fail here
{
    Set-Attr $result "failed" $true;
    Exit-Json $result;
}
else 
{
    $xml = [xml](Get-Content $file) # get the file
}

# Update element using xpath
if ($xpath -ne $Null -and $value -ne $Null -and $addContainer -eq $Null)
{
    Edit-XmlNodes -doc $xml -xpath $xpath -val $value
    $xml.save($file)
    Set-Attr $result "changed" $true;
}

# add a container node 
if ($xpath -ne $Null -and $value -ne $Null -and $addContainer -eq $True)
{ 
    add-Container -doc $xml -xpath $xpath -val $value
    $xml.save($file)
    Set-Attr $result "changed" $true;
}

# Add/Remove elements
if ($xpath -ne $Null -and $value -ne $Null -and $state -ne $Null -and $addContainer -eq $False)
{ 
    if ($state -eq "present")
    {
        add-XmlNodes -doc $xml -xpath $xpath -val $value
        $xml.save($file)
        Set-Attr $result "changed" $true;
    }
#    else if ($state -eq "absent")
#    {
#        remove-XmlNodes -doc $xml -xpath $xpath -val $value
#        $xml.save($file)
#        Set-Attr $result "changed" $true;
#    }
}

# Clear all child nodes
if ($xpath -ne $Null -and $value -eq $Null -and $state -eq "absent" -and $addContainer -eq $False)
{ 
        Clear-Nodes -doc $xml -xpath $xpath
        $xml.save($file)
        Set-Attr $result "changed" $true;
}



#If (($creates -ne $false) -and ($state -ne "absent") -and (Test-Path $creates))
#{
#    Exit-Json $result;
#}

#Set-Attr $result "changed" $true;

Exit-Json $result;


# Status Types
# Set-Attr $result "failed" $true;   // output = failed
# Set-Attr $result "failed" $false;  // output = ok
# Set-Attr $result "changed" $true;  // output = changed
# Set-Attr $result "changed" $false; // output = ok