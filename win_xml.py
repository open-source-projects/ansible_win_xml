#!/usr/bin/python
# -*- coding: utf-8 -*-

## An Ansible XML manipulator for windows host management.
## Tool is in its very early stages. Improvements to come.
## www.gitlab.com/slastrina

DOCUMENTATION = '''
---
module: win_xml
version_added: "1.0"
short_description: An Ansible XML manipulator for windows host management.
description:
    - An Ansible XML manipulator for windows host management.
options:
  file:
    description:
      - Path to xml file (on system being provisioned)
    required: true
    default: null
    choices: []
  xpath:
    description:
      - A valid XPath expression describing the item(s) you want to manipulate. Operates on the document root, C(/), by default.
    required: false
    default: /
    choices: []
  state:
    description:
      - Set or remove an xpath selection (node(s), attribute(s))
    required: false
    default: present
    choices:
      - "absent"
      - "present"
  value:
    description:
      - Desired state of the selected attribute. Either a string, or to unset a value, the Python C(None) keyword (YAML Equivalent, C(null)).
    required: false
    default: Elements default to no value (but present). Attributes default to an empty string.
    choices: []
  add_container:
    description:
      - 'Whether to build an empty container. for creating xml skeletons'
    required: false
    default: null
    choices: []
'''

EXAMPLES = '''

#Editing an attribute within an element
  - name: Set Log Levels 
    win_xml:
      file: "{{ item }}"
      xpath: "//logging/@smartInspectLogCategories"
      value: "{{ log_level_jms }}"
    with_items:
      - c:/foo/bar.xml
      - c:/foo/bar2.xml

#XML string to XML - insert into existing xml node/structure
  - name: Edit Tool Config
    win_xml:
      file: c:/inetpub/wwwroot/app/Configuration/ToolConfiguration.xml
      xpath: "//tool_configuration"
      value: "{{ item }}"
      addContainer: "False"
      state: "present"
    with_items:  
      - <tool_details><tool_name>some tool name</tool_name> <enabled>{{ tool_enable }}</enabled></tool_details>
      - <tool_details><tool_name>some tool name 2</tool_name> <enabled>{{ tool_enable }}</enabled></tool_details>

#Delete all elements within a node	  
  - name: Clearing existing child elements
    win_xml:
      file: c:/Program Files/Synchronoss/spatialSTORM/BIN/StormServiceHostManager.exe.config
      xpath: "{{ item }}"
      addContainer: "False"
      state: "absent"
    with_items:
      - "//system.serviceModel/behaviors/endpointBehaviors"
      - "//system.serviceModel/client"
	
#Add Skeleton	
  - name: Add Queue Top Level Containers
    win_xml:
      file: c:/Program Files/Synchronoss/spatialSTORM/BIN/StormServiceHostManager.exe.config
      xpath: "//system.serviceModel"
      value: "{{ item }}"
      addContainer: "True"
    with_items:
      - client
      - services
'''
