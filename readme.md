## ansible_win_xml

This repo contains a simple XML manipulation module designed to be used with Ansible Windows Hosts. The Module is in its very early stages of development and does not really portray good design as yet. The Tool will improve over time and i welcome collaboration on the project from anyone. 

The ultimate goal will be to submit this as a core/extra module in ansible.

## Reporting bugs

Feel free to report bugs in the gitlab project's issue tracker. I'll respond as soon as i can.

## License

The module is developed under the GPLv3 licence

## Installation

Installation will depend on your folder structure. A full guide to installing modules can be found in the Ansible official documentation.

**Example installation:**
in the base directory of your ansible project, create a 'library' directory (if not already), place both win_xml.py and win_xml.ps1 in this directory

## Tool Usage

See win_xml.py for accurate and latest instructions

**Option**
```
options:
  file:
    description:
      - Path to xml file (on system being provisioned)
    required: true
    default: null
    choices: []
  xpath:
    description:
      - A valid XPath expression describing the item(s) you want to manipulate. Operates on the document root, C(/), by default.
    required: false
    default: /
    choices: []
  state:
    description:
      - Set or remove an xpath selection (node(s), attribute(s))
    required: false
    default: present
    choices:
      - "absent"
      - "present"
  value:
    description:
      - Desired state of the selected attribute. Either a string, or to unset a value, the Python C(None) keyword (YAML Equivalent, C(null)).
    required: false
    default: Elements default to no value (but present). Attributes default to an empty string.
    choices: []
  add_container:
    description:
      - 'Whether to build an empty container. for creating xml skeletons'
    required: false
    default: null
    choices: []

```

**Tasks**
#### Editing an attribute within an element
Notes: 
* '//' xpath seek to
* '@' Attribute to modify

```
  - name: Set Log Levels 
    win_xml:
      file: "{{ item }}"
      xpath: "//logging/@level"
      value: "{{ log_level }}"
    with_items:
      - c:/foo/bar.xml
      - c:/foo/bar2.xml
```

#### 'XML string' to XML element - insert blocks of pretagged XML into existing xml node/structure
Notes: 
* Tool takes in an XML string and places it in the node selected by the xpath
* addContainer must be set to False
* state must be set to present

```
  - name: Edit Tool Config
    win_xml:
      file: c:/inetpub/wwwroot/iisApplication/ToolConfiguration.xml
      xpath: "//tool"
      value: "{{ item }}"
      addContainer: "False"
      state: "present"
    with_items:  
      - <tool_details><tool_name>some tool name</tool_name> <enabled>{{ tool_enable }}</enabled></tool_details>
      - <tool_details><tool_name>some tool name 2</tool_name> <enabled>{{ tool_enable }}</enabled></tool_details>
```

#### Delete all Child elements using xpath  
Notes:   
* AddContainer must be set to false  
* State must be set to "absent"  

```
  - name: Clearing existing child elements
    win_xml:
      file: c:/Program Files/company/program/BIN/someConfig.xml
      xpath: "{{ item }}"
      addContainer: "False"
      state: "absent"
    with_items:
      - "//system/behaviors"
      - "//system/client"
```
  
#### Creates empty xml structure e.g <sometag \>
Notes: 
* addContainer must be True

```
#Add Skeleton   
  - name: Add Queue Top Level Containers
    win_xml:
      file: c:/Program Files/company/program/BIN/someConfig.xml
      xpath: "//system.Model"
      value: "{{ item }}"
      addContainer: "True"
    with_items:
      - client
      - services
```
